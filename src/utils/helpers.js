// search for valid subreddits
async function searchForSubreddits(searchterm,limit){
    const b = 'https://www.reddit.com/subreddits/search.json?include_over_18=on&sort=relevance&q='; //base
    const s = `${searchterm}`; // subreddits
    const p = `&limit=${limit}`; // parameters
    const url = b + s + p;

    try{
        const resp = await fetch(url)
        const json = await resp.json()
        return json;
    }

    catch(err){
        console.log(err);
    }
}
// get all posts from a specified subreddit or subreddits separated by +
async function getSubredditData(subreddits,limit,after,sort,timeframe){
    const b = `https://www.reddit.com/r/`; //base
    const sr = `/${sort}/.json`; // sorting
    const s = `${subreddits.join('+')}`; // subreddits
    const p = `?limit=${limit}&after=${after}&t=${timeframe}`; // parameters
    const url = b +  s + sr + p;

    try{
        const resp = await fetch(url)
        const json = await resp.json()
        return json;
    }
    
    catch(err){
        console.log(err);
    }
}
// a function for debouncing the search input
function debounce (fn, delay) {
    var timeoutID = null
    return function () {
      clearTimeout(timeoutID)
      var args = arguments
      var that = this
      timeoutID = setTimeout(function () {
        fn.apply(that, args)
      }, delay)
    }
  }


export {getSubredditData, searchForSubreddits, debounce}