import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import Home from './views/Home.vue'
import router from './router'

//global styles
import './styles/global.css'

Vue.config.productionTip = false

export const eventBus = new Vue()

new Vue({
  router,
  render: h => h(Home)
}).$mount('#app')
